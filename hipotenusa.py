from math import *

def valor_hipotenusa(cateto_1, cateto_2):
    hipotenusa = sqrt(cateto_1 ** 2 + cateto_2 ** 2)
    return round(hipotenusa, 2)

def principal():
    cateto_1 = float(input("Ingrese un numero: "))
    cateto_2 = float(input("Ingrese un numero: "))
    resultado = valor_hipotenusa(cateto_1, cateto_2)
    print("El valor de la hipotenusa es: ", resultado)

principal()