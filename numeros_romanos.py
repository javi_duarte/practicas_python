num_unidades = [" ", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
num_decenas = [" ", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
num_centenas = [" ", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]

def principal():
    numero = int(input("Ingrese un numero entre 1 y 1000: "))
    if numero > 0 and numero <= 1000:
        convertir_romanos(numero)
    else:
        principal()

def convertir_romanos(numero):
    if numero == 1000:
        print("M")
    else:
        centenas = numero // 100
        decenas = (numero // 10) % 10
        unidades = numero % 10
        print(num_centenas[centenas] + num_decenas[decenas] + num_unidades[unidades])

principal()