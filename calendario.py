# Recibe el año (anho) y devuelve (uA) si es bisiesto o no
def siBisiesto(anho):
    # Si el año es múltiplo de 4
    if anho % 4 == 0 and anho % 100 != 0 or anho % 400 == 0:
        ua = True
    else:
        ua = False
    return ua

# Recibe el año (anho) correspondiente y
# devuelve el día de la semana (diaSemana) en el que cae el primer día de dicho año


def primerDiaAnho(anho):
    if anho == 1901:
        # Si es 1901, el primer día del año es Lunes
        diaSemana = 1
    else:
        # Si el año es mayor que 1901
        # Buscar primer dia del año
        # Se comienza en lunes
        diaSemana = 1
        for i in range(1901, anho+1):
            # Si el año anterior fue bisiesto el día de la semana
            # habrá que sumarle dos más, y sino uno.
            ua = siBisiesto(i-1)
            if ua:
                diaSemana = diaSemana+2
            else:
                diaSemana = diaSemana+1
            # Si se llega a 8, es lunes
            if diaSemana == 8:
                diaSemana = 1
            # Si se llega a 9, es martes
            if diaSemana == 9:
                diaSemana = 2
    return diaSemana

# Recibe el año (anho) y mes (mes) correspondientes y
# devuelve el día de la semana (diaSemana) en el que cae el primer día del mes de ese año


def primerDiaMes(anho, mes):
    # ¿Es un año bisiesto?
    ua = siBisiesto(anho)
    # Primer día de la semana del año
    pds = primerDiaAnho(anho)
    for i in range(1, mes+1):
        # Si es enero, el primer día será el primer día del año
        if i == 1:
            diaSemana = pds
        # Si es marzo, el primer día dependerá de si el año es bisiesto o no
        if i == 3:
            if ua:
                diaSemana = diaSemana+1
            else:
                diaSemana = diaSemana
        # Si es un mes precedido de otro de 31 días, se le sumarán 3 días
        if (i == 2) or (i == 4) or (i == 6) or (i == 8) or (i == 9) or (i == 11):
            diaSemana = diaSemana+3
        # Si es un mes precedido de otro de 30 días, se le sumarán 2 días
        if (i == 5) or (i == 7) or (i == 10) or (i == 12):
            diaSemana = diaSemana+2
        # Si llega a 8, es lunes
        if diaSemana == 8:
            diaSemana = 1
        # Si llega a 9, es martes
        if diaSemana == 9:
            diaSemana = 2
        # Si llega a 10, es miércoles
        if diaSemana == 10:
            diaSemana = 3
    return diaSemana

# Dibuja la hoja de calendario del mes (mes) y año (anho)


def dibujarMes(anho, mes):
    # Dibuja la cabecera de la hoja de calendario
    dibujacabecera(anho, mes)
    # Dibuja los caracteres anteriores a la primera semana
    semana = abreMes(anho, mes)
    # Dibuja la primera semana del mes
    ndm = dibujaPrimSemana(anho, mes, semana)
    udm = ultimoDiaMes(anho, mes)
    # Mientras queden más de 7 días
    while (udm-(ndm-1)) >= 7:
        # Inicializar semana
        semana = ""
        # Dibuja las semanas centrales
        ndm = dibujaSigteSemana(ndm, semana)
    # Si no ha llegado al último día
    if udm != (ndm-1):
        # Dibuja la última semana
        dibujaUltSemana(ndm, anho, mes)

# Recibe el mes (mes) como número y
# devuelve el mismo mes (mT) como texto


def mestexto(mes):
    # Dependiendo del número de mes, se le asigna a mT el texto correspondiente
    if mes == 1:
        mt = "ENERO"
    elif mes == 2:
        mt = "FEBRERO"
    elif mes == 3:
        mt = "MARZO"
    elif mes == 4:
        mt = "ABRIL"
    elif mes == 5:
        mt = "MAYO"
    elif mes == 6:
        mt = "JUNIO"
    elif mes == 7:
        mt = "JULIO"
    elif mes == 8:
        mt = "AGOSTO"
    elif mes == 9:
        mt = "SEPTIEMBRE"
    elif mes == 10:
        mt = "OCTUBRE"
    elif mes == 11:
        mt = "NOVIEMBRE"
    elif mes == 12:
        mt = "DICIEMBRE"
    return mt

# Dibuja la cabecera de la hoja de calendario del mes (mes) y año (anho)


def dibujacabecera(anho, mes):
    # Declara la variable linea, para controlar
    # el número de caracteres que hay que pintar
    # hasta llegar al año
    # Primero un salto de línea
    print(" ")
    # Se le asigna a linea el número de caracteres
    # que tiene el mes correspondiente
    linea = len(mestexto(mes))
    # Se escribe el mes en forma de texto
    print(mestexto(mes), end="")
    # Hasta sumar 22 se escriben espacios
    while linea <= 22:
        print(" ", end="")
        linea = linea+1
    # Se escribe el año
    print(anho)
    # Subrayado doble
    print("===========================")
    # Días de la semana
    print("LU  MA  MI  JU  VI | SA  DO")
    # Otro subrayado doble
    print("===========================")

# Dependiendo del día de la semana en el
# que comience el mes, muestra por pantalla
# los caracteres que corresponden


def abreMes(anho, mes):
    # Primer día del mes
    diaSemana = primerDiaMes(anho, mes)
    # Según el día de la semana que sea
    # se pintan los caracteres que corresponden
    if diaSemana == 2:
        semana = " .  "
    elif diaSemana == 3:
        semana = " .   .  "
    elif diaSemana == 4:
        semana = " .   .   .  "
    elif diaSemana == 5:
        semana = " .   .   .   .  "
    elif diaSemana == 6:
        semana = " .   .   .   .   . | "
    elif diaSemana == 7:
        semana = " .   .   .   .   . |  .  "
    return semana

# Recibe año (anho) y mes (mes), así como
# la variable semana, para guardar los caracteres que siguen, y
# devuelve el número del día del mes (nDM) en el que comienza la semana siguiente y,
# además, pinta la primera semana del mes


def dibujaPrimSemana(anho, mes, semana):
    # Día de la semana del primer día del mes
    ndiaSemana = primerDiaMes(anho, mes)
    # Día 1 del mes
    ndm = 1
    # Hasta que llegamos al domingo...
    for i in range(ndiaSemana, 8):
        # Escribimos un espacio
        semana = semana+" "
        # Escribir el número del día del mes
        semana = semana+str(ndm)
        # Sumar 1 al día del mes y al día de la semana
        ndm = ndm+1
        ndiaSemana = ndiaSemana+1
        if ndiaSemana == 6:
            # Si es sábado
            semana = semana+" |"
        else:
            # El resto de días
            semana = semana+" "
        if i != 7:
            # Otro espacio
            semana = semana+" "
    print(semana)
    return ndm

# Recibe el año (anho) y el mes (mes) y
# devuelve el último día del mes (uDM),
# es decir, el número de días del mes


def ultimoDiaMes(anho, mes):
    mesDias = [int() for i in range(0, 12)]
    # ¿Es año bisiesto?
    ua = siBisiesto(anho)
    if ua:
        # Si lo es, febrero tiene 29 días
        mesDias[2] = 29
    else:
        mesDias[2] = 28
    # Recorrer MesDias y guardar el número de días que tienen el resto de meses
    for i in range(0, 12):
        if (i == 4 or i == 6 or i == 9 or i == 11):
            mesDias[i] = 30
        if (i == 1 or i == 3 or i == 5 or i == 7 or i == 8 or i == 10 or i == 12):
            mesDias[i] = 31
    # El mes (mes) tiene (uDM) días
    udm = mesDias[mes]
    return udm

# Recibe el número del día del mes (nDM) que toca mostrar por pantalla,
# y la variable semana, donde guardar los caracteres a mostrar por pantalla,
# y devuelve, de nuevo, el número del día del mes (nDM), en el que comienza la semana siguiente.
# Además, pinta las semanas que existen entre la primera y la última semana.


def dibujaSigteSemana(ndm, semana):
    # Declarar i para usarlo como contador
    i = int()
    # Escribir la semana
    # Del lunes al domingo
    for i in range(1, 8):
        # Si el número del mes es menor de 10
        if ndm < 10:
            # Escribir un espacio
            semana = semana+" "
        # Escribir el número del día del mes
        semana = semana+str(ndm)
        # Sumar 1 al día del mes
        ndm = ndm+1
        # Si no hemos llegado al viernes o es sábado
        if (i < 5 or i == 6):
            # Escribir dos espacios
            semana = semana+"  "
        # Si es viernes
        if i == 5:
            # Escribir espacio-barra-espacio
            semana = semana+" | "
    # Pintar la semana
    print(semana)
    return ndm

# Recibe como parámetros: el número del día del mes (nDM) que toca mostrar,  por pantalla,
# el año (anho) y el mes (mes). Además, pinta la última semana y
# cierra la hoja de calendario llamando a la función cierraMes.


def dibujaUltSemana(ndm, anho, mes):
    # Último día del mes
    udm = ultimoDiaMes(anho, mes)
    # Número de días que faltan para terminar el mes
    j = (udm-ndm)+1
    # Escribir semana
    semana = ""
    for i in range(1, j+1):
        # Si no ha llegado al domingo
        if i < 7:
            # Escribir día
            semana = semana+str(ndm)
        # Sumar 1 al día del mes
        ndm = ndm+1
        # Si no ha llegado al viernes
        if (i < 5):
            # Escribir espacio
            semana = semana+"  "
        if i == 5:
            # Si es viernes, espacio-barra-espacio
            semana = semana+" | "
    # Cerrar la hoja de calendario con los caracteres que correspondan
    cierraMes(j, semana)

# Dependiendo del día de la semana en el que termine el mes,
# muestra los caracteres que corresponden. Recibe el día de la semana
# en el que acaba el mes y la variable semana, para
# guardar los caracteres que faltan para
# cerrar la hoja de calendario. Además lo muestra por pantalla.


def cierraMes(diaSemana, semana):
    if diaSemana == 1:
        semana = semana+" .   .   .   . |  .   ."
    elif diaSemana == 2:
        semana = semana+" .   .   . |  .   ."
    elif diaSemana == 3:
        semana = semana+" .   . |  .   ."
    elif diaSemana == 4:
        semana = semana+" . |  .   ."
    elif diaSemana == 5:
        semana = semana+" .   ."
    elif diaSemana == 6:
        semana = semana+"   ."
    print(semana)


def calendario():
    s = True
    while s:
        # Pedir por teclado el mes
        print("¿Mes (1..12)? ", end="")
        mes = int(input())
        # Comprueba que el mes sea correcto
        if (mes >= 1 and mes <= 12):
            # Pedir por teclado el año
            print("¿Año (1900...2099)? ", end="")
            anho = int(input())
            # Comprueba que el año sea correcto
            if (anho < 1901 or anho > 2099):
                print("Año incorrecto")
            else:
                # Dibuja la hoja de calendario
                dibujarMes(anho, mes)
                print(" ")
        else:
            print("Mes incorrecto")
        # Da la opción de seguir o salir del programa
        #print("Para continuar pulse la tecla S ... ")
        exit()
        seguir = input()
        if ((seguir == "S") or (seguir == "s")):
            s = True
        else:
            s = False

print("----Javier Duarte----2021")
calendario()
