'''Un número primo es un número entero mayor que 1 que solo es divisible por uno y por sí mismo.
Escriba una función que determine si su parámetro es primo o no, devolviendo True si lo es y False en caso contrario.
Escriba un programa principal que lea un número entero del usuario y muestre un mensaje que indique si es primo o no.
Asegúrese de que el programa principal no se ejecutará si el archivo que contiene su solución se importa
a otro programa.'''
#La siguiente función comprueba si un número es divisible por otro,
#empezando por divisor = 2 hasta llegar al número en cuestión.
def es_primo(numero, divisor = 2):
    if divisor >= numero:
        print("Es primo")
        return True
    elif numero % divisor != 0:
        print(":", divisor + 1)
        return es_primo(numero, divisor + 1)

    else:
        print("No es primo." ,divisor, "es divisor")
        return False

def principal():
    numero = int(input("Ingrese un numero entero: "))
    divisor = 2
    resultado = es_primo(numero, divisor)
    print(resultado)
principal()
